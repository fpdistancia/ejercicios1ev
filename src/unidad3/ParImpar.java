package unidad3;

import java.util.Scanner;

public class ParImpar {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Dime un n�mero: ");
		int n = in.nextInt();
		if (n % 2 == 0) 
			System.out.println("El n�mero es par");
		else
			System.out.println("El n�mero es impar");
		
		in.close();
	}

}
