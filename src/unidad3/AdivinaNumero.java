package unidad3;

import java.util.Random;
import java.util.Scanner;

public class AdivinaNumero {

	public static void main(String[] args) {
		Random r = new Random();
		Scanner in = new Scanner(System.in);
		System.out.println(in.delimiter());
		String jugar;
		do {
			System.out.println("�Quieres jugar a adivinar un n�mero? (si/no)");
			jugar = in.nextLine();
			if (!jugar.equals("si") && !jugar.equals("no"))
				System.out.println("Respuesta incorrecta");
		} while (!(jugar.equals("si") || jugar.equals("no")));
		while (jugar.equals("si")) {
			int max;
			max = r.nextInt(99001) + 1000;
			int n;
			n = r.nextInt(max) + 1;
			int nu;
			do {
				System.out.printf("�Qu� n�mero estoy pensando entre 1 y %d? %d ", max, n);
				nu = in.nextInt();
				if (n > nu)
					System.out.println("El n�mero que estoy pensando es mayor");
				else if (n < nu)
					System.out.println("El n�mero que estoy pensando es menor");
			} while (n != nu);
			System.out.println("Felicidades, has acertado. �Quieres jugar otra vez? (si/no)");
			do {
				System.out.println("�Quieres jugar otra vez? (si/no)");
				jugar = in.nextLine();
				if (!jugar.equals("si") && !jugar.equals("no"))
					System.out.println("Respuesta incorrecta");
			} while (!(jugar.equals("si") || jugar.equals("no")));
		}
	}

}
